/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.graph.fed;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementAssign;
import com.hp.hpl.jena.sparql.syntax.ElementBind;
import com.hp.hpl.jena.sparql.syntax.ElementData;
import com.hp.hpl.jena.sparql.syntax.ElementDataset;
import com.hp.hpl.jena.sparql.syntax.ElementExists;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementMinus;
import com.hp.hpl.jena.sparql.syntax.ElementNamedGraph;
import com.hp.hpl.jena.sparql.syntax.ElementNotExists;
import com.hp.hpl.jena.sparql.syntax.ElementOptional;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;
import com.hp.hpl.jena.sparql.syntax.ElementService;
import com.hp.hpl.jena.sparql.syntax.ElementSubQuery;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import com.hp.hpl.jena.sparql.syntax.ElementVisitor;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import gr.tuc.music.sparqlrw.mapping.model.Ontology;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Transforms a graph pattern into a federated one based on the appearing
 * ontology terms and a set of SPARQL endpoints.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class GraphFedConverter implements ElementVisitor {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GraphFedConverter.class);

    private Element result;
    private MappingModel model;
    private HashMap<String, List<String>> ontEndpointMap;

    /**
     * Transforms a graph pattern into a federated one based on the appearing
     * ontology terms and a set of SPARQL endpoints.
     *
     * @param element        A graph pattern.
     * @param model          A mapping model.
     * @param ontEndpointMap The endpoints of the integrated data sources. The
     *                       keys are ontology uris and the values SPARQL
     *                       endpoint urls.
     * @return The transformed graph pattern.
     */
    public Element convert(Element element, MappingModel model, HashMap<String, List<String>> ontEndpointMap) {

        if ((element == null) || (model == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        result = null;
        this.model = model;
        this.ontEndpointMap = ontEndpointMap;

        // rewrite input graph pattern
        element.visit(this);

        return result;
    }

    public void visit(ElementTriplesBlock el) {
        ElementGroup element = new ElementGroup();
        List<TripleRoute> tripleRouteList = new ArrayList<TripleRoute>();

        // Triple    Endpoints    Strategy
        // t1     -> E1 E2 E3     Executed alone over E1 E2 E3.
        // t2     -> E1 E3        Executed alone over E1 E3.
        // t3, t4 -> E1           Executed together over E1.
        // t5     -> E4           Executed alone over E4.
        // t6     -> empty        Not enclosed in any service clause.

        // categorize triples and endpoints
        for (Triple triple : el.getPattern()) {
            List<String> endpointList = getEndpoints(triple);

            boolean added = false;
            for (TripleRoute route : tripleRouteList) {
                if (route.hasSameEndpoints(endpointList)) {
                    route.addTriple(triple);
                    added = true;
                    break;
                }
            }

            if (!added) {
                tripleRouteList.add(new TripleRoute(triple, endpointList));
            }
        }

        for (TripleRoute route : tripleRouteList) {
            if (route.getEndpointList().size() > 1) {
                // (SERVICE <endpoint1> {triple1, ..., tripleK}) UNION ... (SERVICE <endpointN> {triple1, ..., tripleK})
                ElementUnion eu = new ElementUnion();
                for (String endpoint : route.getEndpointList()) {
                    ElementTriplesBlock etb = new ElementTriplesBlock();
                    for (Triple triple : route.getTripleList()) {
                        etb.addTriple(triple);
                    }
                    eu.addElement(new ElementService(endpoint, etb));
                }

                element.addElement(eu);
            } else if (route.getEndpointList().size() == 1) {
                // SERVICE <endpoint> {triple1 triple2 ... tripleN}
                ElementTriplesBlock etb = new ElementTriplesBlock();
                for (Triple triple : route.getTripleList()) {
                    etb.addTriple(triple);
                }
                element.addElement(new ElementService(route.getEndpointList().get(0), etb));
            } else {
                // {triple1 triple2 ... tripleN}
                ElementTriplesBlock etb = new ElementTriplesBlock();
                for (Triple triple : route.getTripleList()) {
                    etb.addTriple(triple);
                }
                element.addElement(etb);
            }
        }

        result = element;
    }

    public void visit(ElementPathBlock el) {
        // should never get here
        result = el;
    }

    public void visit(ElementFilter el) {
        result = el;
    }

    public void visit(ElementAssign el) {
        result = el;
    }

    public void visit(ElementBind el) {
        result = el;
    }

    public void visit(ElementData el) {
        result = el;
    }

    public void visit(ElementUnion el) {
        ElementUnion element = new ElementUnion();

        for (Element e : el.getElements()) {
            e.visit(this);
            element.addElement(result);
        }

        result = element;
    }

    public void visit(ElementOptional el) {
        el.getOptionalElement().visit(this);
        result = new ElementOptional(result);
    }

    public void visit(ElementGroup el) {
        ElementGroup element = new ElementGroup();

        for (Element e : el.getElements()) {
            e.visit(this);
            element.addElement(result);
        }

        result = element;
    }

    public void visit(ElementDataset el) {
        el.getPatternElement().visit(this);
        result = new ElementDataset(el.getDataset(), result);
    }

    public void visit(ElementNamedGraph el) {
        el.getElement().visit(this);
        result = new ElementNamedGraph(el.getGraphNameNode(), result);
    }

    public void visit(ElementExists el) {
        el.getElement().visit(this);
        result = new ElementExists(result);
    }

    public void visit(ElementNotExists el) {
        el.getElement().visit(this);
        result = new ElementNotExists(result);
    }

    public void visit(ElementMinus el) {
        el.getMinusElement().visit(this);
        result = new ElementMinus(result);
    }

    public void visit(ElementService el) {
        el.getElement().visit(this);
        result = new ElementService(el.getServiceNode(), result, el.getSilent());
    }

    public void visit(ElementSubQuery el) {
        Query query = el.getQuery();
        query.getQueryPattern().visit(this);
        query.setQueryPattern(result);
        result = new ElementSubQuery(query);
    }

    /**
     * Returns a list of endpoints where a specific triple has to be executed.
     *
     * @param triple A triple.
     * @returns A list of endpoints.
     */
    private List<String> getEndpoints(Triple triple) {
        List<String> endpointList = new ArrayList<String>();

        Node subject = triple.getSubject();
        Node predicate = triple.getPredicate();
        Node object = triple.getObject();

        // search endpoints for subject
        if (subject.isURI() && !isVocabTerm(subject)) {
            String ontology = model.resolveOntology(subject.getURI());
            if (ontology != null) {
                List<String> tempList = ontEndpointMap.get(ontology);
                transferValues(tempList, endpointList);
            }
        }

        // search endpoints for predicate
        if (predicate.isURI()) {
            String ontology = model.resolveOntology(predicate.getURI());
            if (ontology != null) {
                List<String> tempList = ontEndpointMap.get(ontology);
                transferValues(tempList, endpointList);
            }
        }

        // search endpoints for object
        if (object.isURI() && !isVocabTerm(object)) {
            String ontology = model.resolveOntology(object.getURI());
            if (ontology != null) {
                List<String> tempList = ontEndpointMap.get(ontology);
                transferValues(tempList, endpointList);
            }
        }


        // in case the list is empty execute the triple over all the integrated
        // data sources
        if (endpointList.isEmpty()) {
            for (Ontology ontology : model.getLocalOntologyList()) {
                List<String> tempList = ontEndpointMap.get(ontology.getUri());
                transferValues(tempList, endpointList);
            }
        }

        return endpointList;
    }

    /**
     * Transfers the elements of a source list to a target list maintaining
     * unique values.
     *
     * @param from A source list.
     * @param to   A target list.
     */
    private void transferValues(List<String> from, List<String> to) {
        if ((from == null) || (to == null)) {
            return;
        }

        for (String value : from) {
            if (!to.contains(value)) {
                to.add(value);
            }
        }
    }

    /**
     * Checks whether or not a given node is a built-in vocabulary term.
     *
     * @param node A node.
     * @return True in case the input node is a built-in vocabulary term,
     *         otherwise false.
     */
    private boolean isVocabTerm(Node node) {
        if (node.isURI()) {
            String uri = node.getURI();
            if (uri.startsWith(ARQConstants.rdfPrefix) || uri.startsWith(ARQConstants.rdfsPrefix) || uri.startsWith(ARQConstants.owlPrefix)) {
                return true;
            }
        }

        return false;
    }
}
