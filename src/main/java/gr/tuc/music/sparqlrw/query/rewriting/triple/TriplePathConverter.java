/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.TriplePath;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.path.P_Alt;
import com.hp.hpl.jena.sparql.path.P_Distinct;
import com.hp.hpl.jena.sparql.path.P_FixedLength;
import com.hp.hpl.jena.sparql.path.P_Inverse;
import com.hp.hpl.jena.sparql.path.P_Link;
import com.hp.hpl.jena.sparql.path.P_Mod;
import com.hp.hpl.jena.sparql.path.P_Multi;
import com.hp.hpl.jena.sparql.path.P_NegPropSet;
import com.hp.hpl.jena.sparql.path.P_OneOrMore1;
import com.hp.hpl.jena.sparql.path.P_OneOrMoreN;
import com.hp.hpl.jena.sparql.path.P_ReverseLink;
import com.hp.hpl.jena.sparql.path.P_Seq;
import com.hp.hpl.jena.sparql.path.P_Shortest;
import com.hp.hpl.jena.sparql.path.P_ZeroOrMore1;
import com.hp.hpl.jena.sparql.path.P_ZeroOrMoreN;
import com.hp.hpl.jena.sparql.path.P_ZeroOrOne;
import com.hp.hpl.jena.sparql.path.PathVisitor;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import gr.tuc.music.sparqlrw.query.rewriting.utils.VarGenerator;
import org.slf4j.LoggerFactory;

/**
 * Transforms a triple pattern, containing a property path in its predicate
 * position, into an equivalent graph pattern consisted of simple triple
 * patterns.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class TriplePathConverter implements PathVisitor {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(TriplePathConverter.class);

    private Element result;
    private Node subject;
    private Node object;

    /**
     * Transforms a triple pattern containing a property path in its predicate
     * position into an equivalent graph pattern consisted of simple triple
     * patterns.
     *
     * @param triple A triple pattern containing a property path in its
     *               predicate position.
     * @return The transformed graph pattern.
     */
    public Element convert(TriplePath triple) {

        if (triple == null) {
            log.warn("Input parameter cannot be null.");
            return null;
        }

        // initialize class attributes
        result = null;
        subject = triple.getSubject();
        object = triple.getObject();

        // rewrite input graph pattern
        triple.getPath().visit(this);

        return result;
    }

    public void visit(P_Link pathNode) {
        // Case: (sub, uri, ob) = (sub, uri, ob)
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, pathNode.getNode(), object);
        element.addTriple(triple);
        result = element;
    }

    public void visit(P_ReverseLink pathNode) {
        // Case: (sub, ^uri, ob) = (ob, uri, sub)
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(object, pathNode.getNode(), subject);
        element.addTriple(triple);
        result = element;
    }

    public void visit(P_NegPropSet pathNotOneOf) {
        // Case: (sub, !(path1, ..., pathn), ob) = unsupported
        log.warn("Negated property path is not supported.");
        throw new UnsupportedOperationException("Negated property path is not supported.");
    }

    public void visit(P_Inverse inversePath) {
        // Case: (sub, ^path, ob) = (ob, path, sub)
        TriplePathConverter transformer = new TriplePathConverter();

        TriplePath triple = new TriplePath(object, inversePath.getSubPath(), subject);
        result = transformer.convert(triple);
    }

    public void visit(P_Mod pathMod) {
        // Case: (sub, path{N,M}, ob) = (sub, path, ob) - not fully supported
        pathMod.getSubPath().visit(this);
    }

    public void visit(P_FixedLength pFixedLength) {
        // Case: (sub, path{N}, ob) = (sub, path, ob) - not fully supported
        pFixedLength.getSubPath().visit(this);
    }

    public void visit(P_Distinct pathDistinct) {
        pathDistinct.getSubPath().visit(this);
    }

    public void visit(P_Multi pathMulti) {
        pathMulti.getSubPath().visit(this);
    }

    public void visit(P_Shortest pathShortest) {
        pathShortest.getSubPath().visit(this);
    }

    public void visit(P_ZeroOrOne path) {
        // Case: (sub, path?, ob) = (sub, path, ob) - not fully supported
        path.getSubPath().visit(this);
    }

    public void visit(P_ZeroOrMore1 path) {
        // Case: (sub, path*, ob) = (sub, path, ob) - not fully supported
        path.getSubPath().visit(this);
    }

    public void visit(P_ZeroOrMoreN path) {
        // Case: (sub, path*, ob) = (sub, path, ob) - not fully supported
        path.getSubPath().visit(this);
    }

    public void visit(P_OneOrMore1 path) {
        // Case: (sub, path+, ob) = (sub, path, ob) - not fully supported
        path.getSubPath().visit(this);
    }

    public void visit(P_OneOrMoreN path) {
        // Case: (sub, path+, ob) = (sub, path, ob) - not fully supported
        path.getSubPath().visit(this);
    }

    public void visit(P_Alt pathAlt) {
        // Case: (sub, path1|path2, ob) = (sub, path1, ob) UNION (sub, path2, ob)
        ElementUnion element = new ElementUnion();

        pathAlt.getLeft().visit(this);
        element.addElement(result);

        pathAlt.getRight().visit(this);
        element.addElement(result);

        result = element;
    }

    public void visit(P_Seq pathSeq) {
        // Case: (sub, path1/path2, ob) = (sub, path1, ?var) AND (?var, path2, ob)
        ElementGroup element = new ElementGroup();
        TriplePathConverter transformer = new TriplePathConverter();

        Var variable = VarGenerator.generate();
        TriplePath triple = new TriplePath(subject, pathSeq.getLeft(), variable);
        element.addElement(transformer.convert(triple));

        triple = new TriplePath(variable, pathSeq.getRight(), object);
        element.addElement(transformer.convert(triple));

        result = element;
    }
}
