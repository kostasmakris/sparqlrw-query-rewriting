/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple;

import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.syntax.Element;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;

/**
 * Rewrites a triple pattern based on a set of predefined GAV mappings.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public interface TripleRewriter {

    public abstract Element rewrite(Triple triple, MappingModel model);

    public abstract Element rewrite(Element element, MappingModel model);
}
