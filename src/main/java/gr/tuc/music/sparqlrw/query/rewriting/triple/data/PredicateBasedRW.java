/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple.data;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.expr.E_Datatype;
import com.hp.hpl.jena.sparql.expr.E_Equals;
import com.hp.hpl.jena.sparql.expr.E_Function;
import com.hp.hpl.jena.sparql.expr.E_GreaterThan;
import com.hp.hpl.jena.sparql.expr.E_GreaterThanOrEqual;
import com.hp.hpl.jena.sparql.expr.E_LessThan;
import com.hp.hpl.jena.sparql.expr.E_LessThanOrEqual;
import com.hp.hpl.jena.sparql.expr.E_LogicalAnd;
import com.hp.hpl.jena.sparql.expr.E_LogicalNot;
import com.hp.hpl.jena.sparql.expr.E_LogicalOr;
import com.hp.hpl.jena.sparql.expr.E_NotEquals;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprList;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementMinus;
import com.hp.hpl.jena.sparql.syntax.ElementSubQuery;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import gr.tuc.music.sparqlrw.mapping.model.Expression;
import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitorBase;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.condition.AndCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.BasicCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.Condition;
import gr.tuc.music.sparqlrw.mapping.model.condition.ConditionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.condition.NotCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.OrCondition;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import gr.tuc.music.sparqlrw.mapping.model.datarange.XmlDatatype;
import gr.tuc.music.sparqlrw.mapping.model.predicate.BinaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.predicate.Operator;
import gr.tuc.music.sparqlrw.mapping.model.predicate.PredicateVisitor;
import gr.tuc.music.sparqlrw.mapping.model.predicate.UnaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyTransform;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyTransitive;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import gr.tuc.music.sparqlrw.query.rewriting.utils.NodeUtils;
import gr.tuc.music.sparqlrw.query.rewriting.utils.VarGenerator;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a data triple pattern based on a mapping of the term appearing in
 * its predicate position.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class PredicateBasedRW extends ExpressionVisitorBase<Element> implements ConditionVisitor<Expr, Expr>, PredicateVisitor<Expr, List<Expr>> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(PredicateBasedRW.class);

    private ElementTriplesBlock defaultValue;

    private Node subject;
    private Node object;

    /**
     * Rewrites a data triple pattern based on a mapping of the term appearing
     * in its predicate position.
     *
     * @param triple  A data triple pattern.
     * @param mapping A mapping for the data triple pattern predicate part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Mapping mapping) {

        if (mapping == null) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        return rewrite(triple, mapping.getExpr2());
    }

    /**
     * Rewrites a data triple pattern based on a mapping of the term appearing
     * in its predicate position.
     *
     * @param triple     A data triple pattern.
     * @param expression A mapping for the data triple pattern predicate part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Expression expression) {

        if ((triple == null) || (expression == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        defaultValue = new ElementTriplesBlock();
        defaultValue.addTriple(triple);

        subject = triple.getSubject();
        object = triple.getObject();

        // visit the right side expression
        return expression.accept(this);
    }

    @Override
    public Element visit(OPropertyUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, NodeFactory.createURI(o.getUri()), object);
        element.addTriple(triple);
        return element;
    }

    @Override
    public Element visit(OPropertyUnion o) {
        ElementUnion element = new ElementUnion();

        for (OPropertyExpression expr : o.getOperandList()) {
            element.addElement(expr.accept(this));
        }

        return element;
    }

    @Override
    public Element visit(OPropertyIntersection o) {
        ElementGroup element = new ElementGroup();

        for (OPropertyExpression expr : o.getOperandList()) {
            element.addElement(expr.accept(this));
        }

        return element;
    }

    @Override
    public Element visit(OPropertyDifference o) {
        ElementGroup element = new ElementGroup();
        List<OPropertyExpression> operandList = o.getOperandList();

        element.addElement(operandList.get(0).accept(this));

        for (OPropertyExpression expr : operandList.subList(1, operandList.size())) {
            element.addElement(new ElementMinus(expr.accept(this)));
        }

        return element;
    }

    @Override
    public Element visit(OPropertyComposition o) {
        ElementGroup element = new ElementGroup();
        List<OPropertyExpression> operandList = o.getOperandList();

        Var var1 = VarGenerator.generate();
        Var var2;

        // rewrite triple (sub, R, ?vari-1) by predicate
        Triple triple = Triple.create(subject, Node.ANY, var1);
        PredicateBasedRW predicateRW = new PredicateBasedRW();
        element.addElement(predicateRW.rewrite(triple, operandList.get(0)));

        for (OPropertyExpression expr : operandList.subList(1, operandList.size() - 1)) {
            var2 = VarGenerator.generate();

            // rewrite triple (?vari-1, R, ?vari) by predicate
            triple = Triple.create(var1, Node.ANY, var2);
            predicateRW = new PredicateBasedRW();
            element.addElement(predicateRW.rewrite(triple, expr));

            var1 = var2;
        }

        // rewrite triple (?vari, R, ob) by predicate
        triple = Triple.create(var1, Node.ANY, object);
        predicateRW = new PredicateBasedRW();
        element.addElement(predicateRW.rewrite(triple, operandList.get(operandList.size() - 1)));

        return element;
    }

    @Override
    public Element visit(OPropertyInverse o) {
        Triple triple = Triple.create(object, Node.ANY, subject);
        PredicateBasedRW predicateRW = new PredicateBasedRW();
        return predicateRW.rewrite(triple, o.getOperand());
    }

    @Override
    public Element visit(OPropertyTransitive o) {
        ElementUnion element = new ElementUnion();

        // create a property composition using the input transitive property
        List<OPropertyExpression> propertyList = new ArrayList<OPropertyExpression>();
        propertyList.add(o.getOperand());
        propertyList.add(o.getOperand());

        OPropertyComposition property = new OPropertyComposition(propertyList);

        element.addElement(o.getOperand().accept(this));
        element.addElement(property.accept(this));

        return element;
    }

    @Override
    public Element visit(OPropertyRestriction o) {
        ElementGroup element = new ElementGroup();

        element.addElement(o.getProperty().accept(this));

        if (o.getDomain() != null) {
            Triple triple = Triple.create(subject, NodeUtils.rdfType, Node.ANY);
            ObjectBasedRW objectRW = new ObjectBasedRW();
            element.addElement(objectRW.rewrite(triple, o.getDomain()));
        }
        if (o.getRange() != null) {
            Triple triple = Triple.create(object, NodeUtils.rdfType, Node.ANY);
            ObjectBasedRW objectRW = new ObjectBasedRW();
            element.addElement(objectRW.rewrite(triple, o.getRange()));
        }

        return element;
    }

    @Override
    public Element visit(OPropertyExistPredRestriction o) {
        ElementGroup element = new ElementGroup();

        Var var1 = VarGenerator.generate();
        Triple triple1 = Triple.create(subject, Node.ANY, var1);
        PredicateBasedRW predicateRW = new PredicateBasedRW();
        element.addElement(predicateRW.rewrite(triple1, o.getOperand1List().get(0)));

        Var var2 = VarGenerator.generate();
        Triple triple2 = Triple.create(object, Node.ANY, var2);
        predicateRW = new PredicateBasedRW();
        element.addElement(predicateRW.rewrite(triple2, o.getOperand2List().get(0)));

        List<Expr> exprList = new ArrayList<Expr>();
        exprList.add(new ExprVar(var1));
        exprList.add(new ExprVar(var2));

        // create a filter specifying the predicate condition between the
        // variables in the variable list and add it to the group graph pattern
        ElementFilter filter = new ElementFilter(o.getPredicate().accept(this, exprList));
        element.addElement(filter);

        return element;
    }

    @Override
    public Element visit(DPropertyUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, NodeFactory.createURI(o.getUri()), object);
        element.addTriple(triple);
        return element;
    }

    @Override
    public Element visit(DPropertyUnion o) {
        ElementUnion element = new ElementUnion();

        for (DPropertyExpression expr : o.getOperandList()) {
            element.addElement(expr.accept(this));
        }

        return element;
    }

    @Override
    public Element visit(DPropertyIntersection o) {
        ElementGroup element = new ElementGroup();

        for (DPropertyExpression expr : o.getOperandList()) {
            element.addElement(expr.accept(this));
        }

        return element;
    }

    @Override
    public Element visit(DPropertyDifference o) {
        ElementGroup element = new ElementGroup();
        List<DPropertyExpression> operandList = o.getOperandList();

        element.addElement(operandList.get(0).accept(this));

        for (DPropertyExpression expr : operandList.subList(1, operandList.size())) {
            element.addElement(new ElementMinus(expr.accept(this)));
        }

        return element;
    }

    @Override
    public Element visit(DPropertyComposition o) {
        ElementGroup element = new ElementGroup();
        List<OPropertyExpression> initOperandList = o.getInitOperandList();

        Var var1 = VarGenerator.generate();
        Var var2;

        // rewrite triple (sub, R, ?vari-1) by predicate
        Triple triple = Triple.create(subject, Node.ANY, var1);
        PredicateBasedRW predicateRW = new PredicateBasedRW();
        element.addElement(predicateRW.rewrite(triple, initOperandList.get(0)));

        for (OPropertyExpression expr : initOperandList.subList(1, initOperandList.size())) {
            var2 = VarGenerator.generate();

            // rewrite triple (?vari-1, R, ?vari) by predicate
            triple = Triple.create(var1, Node.ANY, var2);
            predicateRW = new PredicateBasedRW();
            element.addElement(predicateRW.rewrite(triple, expr));

            var1 = var2;
        }

        // rewrite triple (?vari, U, ob) by predicate
        triple = Triple.create(var1, Node.ANY, object);
        predicateRW = new PredicateBasedRW();
        element.addElement(predicateRW.rewrite(triple, o.getLastOperand()));

        return element;
    }

    @Override
    public Element visit(DPropertyTransform o) {

        if (!object.isVariable()) {
            return defaultValue;
        }

        Var variable = VarGenerator.generate();
        Triple triple = Triple.create(subject, Node.ANY, variable);
        PredicateBasedRW predicateRW = new PredicateBasedRW();
        Element e = predicateRW.rewrite(triple, o.getProperty());

        Expr expr = new ExprVar(variable);
        ExprList exprList = new ExprList(expr);

        if (o.getDatatype() == XmlDatatype.BOOLEAN) {
            expr = new E_Function(XSDDatatype.XSDboolean.getURI(), exprList);
        } else if (o.getDatatype() == XmlDatatype.DATETIME) {
            expr = new E_Function(XSDDatatype.XSDdateTime.getURI(), exprList);
        } else if (o.getDatatype() == XmlDatatype.DECIMAL) {
            expr = new E_Function(XSDDatatype.XSDdecimal.getURI(), exprList);
        } else if (o.getDatatype() == XmlDatatype.DOUBLE) {
            expr = new E_Function(XSDDatatype.XSDdouble.getURI(), exprList);
        } else if (o.getDatatype() == XmlDatatype.FLOAT) {
            expr = new E_Function(XSDDatatype.XSDfloat.getURI(), exprList);
        } else if (o.getDatatype() == XmlDatatype.INTEGER) {
            expr = new E_Function(XSDDatatype.XSDinteger.getURI(), exprList);
        } else {
            expr = new E_Function(XSDDatatype.XSDstring.getURI(), exprList);
        }

        // create the resulted graph pattern (subquery)
        Query query = new Query();
        query.setQueryPattern(e);
        query.addResultVar(subject);
        query.addResultVar(object, expr);

        return new ElementSubQuery(query);
    }

    @Override
    public Element visit(DPropertyRestriction o) {
        ElementGroup element = new ElementGroup();

        element.addElement(o.getProperty().accept(this));

        if (o.getDomain() != null) {
            Triple triple = Triple.create(subject, NodeUtils.rdfType, Node.ANY);
            ObjectBasedRW objectRW = new ObjectBasedRW();
            element.addElement(objectRW.rewrite(triple, o.getDomain()));
        }
        if (o.getRange() != null) {
            element.addElement(o.getRange().accept(this));
        }

        return element;
    }

    @Override
    public Element visit(Datatype o) {
        ElementFilter element;
        Expr expr = new ExprVar(object);
        Expr expr1 = null;
        Expr expr2 = null;

        if (o.getBase() != null) {
            NodeValue value;

            if (o.getBase() == XmlDatatype.BOOLEAN) {
                value = NodeValue.makeString(XSDDatatype.XSDboolean.getURI());
            } else if (o.getBase() == XmlDatatype.DATETIME) {
                value = NodeValue.makeString(XSDDatatype.XSDdateTime.getURI());
            } else if (o.getBase() == XmlDatatype.DECIMAL) {
                value = NodeValue.makeString(XSDDatatype.XSDdecimal.getURI());
            } else if (o.getBase() == XmlDatatype.DOUBLE) {
                value = NodeValue.makeString(XSDDatatype.XSDdouble.getURI());
            } else if (o.getBase() == XmlDatatype.FLOAT) {
                value = NodeValue.makeString(XSDDatatype.XSDfloat.getURI());
            } else if (o.getBase() == XmlDatatype.INTEGER) {
                value = NodeValue.makeString(XSDDatatype.XSDinteger.getURI());
            } else {
                value = NodeValue.makeString(XSDDatatype.XSDstring.getURI());
            }

            expr1 = new E_Equals(new E_Datatype(expr), value);
        }

        if (o.getRestriction() != null) {
            expr2 = o.getRestriction().accept(this, expr);
        }

        if ((expr1 != null) && (expr2 != null)) {
            element = new ElementFilter(new E_LogicalAnd(expr1, expr2));
        } else if (expr1 != null) {
            element = new ElementFilter(expr1);
        } else {
            element = new ElementFilter(expr2);
        }

        return element;
    }

    @Override
    public Expr visit(BasicCondition c, Expr o) {
        List<Expr> exprList = new ArrayList<Expr>();
        exprList.add(o);
        return c.getRestriction().accept(this, exprList);
    }

    @Override
    public Expr visit(AndCondition c, Expr o) {
        List<Condition> operandList = c.getOperandList();
        Expr expr1 = operandList.get(0).accept(this, o);

        for (Condition condition : operandList.subList(1, operandList.size())) {
            Expr expr2 = condition.accept(this, o);
            expr1 = new E_LogicalAnd(expr1, expr2);
        }

        return expr1;
    }

    @Override
    public Expr visit(OrCondition c, Expr o) {
        List<Condition> operandList = c.getOperandList();
        Expr expr1 = operandList.get(0).accept(this, o);

        for (Condition condition : operandList.subList(1, operandList.size())) {
            Expr expr2 = condition.accept(this, o);
            expr1 = new E_LogicalOr(expr1, expr2);
        }

        return expr1;
    }

    @Override
    public Expr visit(NotCondition c, Expr o) {
        return new E_LogicalNot(c.getOperand().accept(this, o));
    }

    public Expr visit(BinaryPredicate p, List<Expr> o) {
        Expr expr;
        Expr expr1 = o.get(0);
        Expr expr2 = o.get(1);

        if (p.getOperator() == Operator.EQUAL) {
            expr = new E_Equals(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER) {
            expr = new E_GreaterThan(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER_EQUAL) {
            expr = new E_GreaterThanOrEqual(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS) {
            expr = new E_LessThan(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS_EQUAL) {
            expr = new E_LessThanOrEqual(expr1, expr2);
        } else {
            expr = new E_NotEquals(expr1, expr2);
        }

        return expr;
    }

    public Expr visit(UnaryPredicate p, List<Expr> o) {
        Expr expr;
        Expr expr1 = o.get(0);
        Expr expr2 = NodeValue.parse(p.getValue().toString());

        if (p.getOperator() == Operator.EQUAL) {
            expr = new E_Equals(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER) {
            expr = new E_GreaterThan(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER_EQUAL) {
            expr = new E_GreaterThanOrEqual(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS) {
            expr = new E_LessThan(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS_EQUAL) {
            expr = new E_LessThanOrEqual(expr1, expr2);
        } else {
            expr = new E_NotEquals(expr1, expr2);
        }

        return expr;
    }
}
