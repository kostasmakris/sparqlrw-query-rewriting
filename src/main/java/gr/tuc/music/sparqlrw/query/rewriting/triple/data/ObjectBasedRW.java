/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple.data;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingFactory;
import com.hp.hpl.jena.sparql.expr.E_Equals;
import com.hp.hpl.jena.sparql.expr.E_GreaterThan;
import com.hp.hpl.jena.sparql.expr.E_GreaterThanOrEqual;
import com.hp.hpl.jena.sparql.expr.E_LessThan;
import com.hp.hpl.jena.sparql.expr.E_LessThanOrEqual;
import com.hp.hpl.jena.sparql.expr.E_NotEquals;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.expr.aggregate.AggCountVarDistinct;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementData;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementMinus;
import com.hp.hpl.jena.sparql.syntax.ElementSubQuery;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import gr.tuc.music.sparqlrw.mapping.model.Expression;
import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitorBase;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassCardinalityRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassEnumeration;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistQuantification;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.predicate.BinaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.predicate.Operator;
import gr.tuc.music.sparqlrw.mapping.model.predicate.PredicateVisitor;
import gr.tuc.music.sparqlrw.mapping.model.predicate.UnaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.query.rewriting.utils.VarGenerator;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a data triple pattern based on a mapping of the term appearing in
 * its object position.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class ObjectBasedRW extends ExpressionVisitorBase<Element> implements PredicateVisitor<Expr, List<Expr>> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ObjectBasedRW.class);

    private ElementTriplesBlock defaultValue;

    private Node subject;
    private Node predicate;

    /**
     * Rewrites a data triple pattern based on a mapping of the term appearing
     * in its object position.
     *
     * @param triple  A data triple pattern.
     * @param mapping A mapping for the data triple pattern object part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Mapping mapping) {

        if (mapping == null) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        return rewrite(triple, mapping.getExpr2());
    }

    /**
     * Rewrites a data triple pattern based on a mapping of the term appearing
     * in its object position.
     *
     * @param triple     A data triple pattern.
     * @param expression A mapping for the data triple pattern object part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Expression expression) {

        if ((triple == null) || (expression == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        defaultValue = new ElementTriplesBlock();
        defaultValue.addTriple(triple);

        subject = triple.getSubject();
        predicate = triple.getPredicate();

        // visit the right side expression
        return expression.accept(this);
    }

    @Override
    public Element visit(ClassUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, predicate, NodeFactory.createURI(o.getUri()));
        element.addTriple(triple);
        return element;
    }

    @Override
    public Element visit(ClassUnion o) {
        ElementUnion element = new ElementUnion();

        for (ClassExpression expr : o.getOperandList()) {
            element.addElement(expr.accept(this));
        }

        return element;
    }

    @Override
    public Element visit(ClassIntersection o) {
        ElementGroup element = new ElementGroup();

        for (ClassExpression expr : o.getOperandList()) {
            element.addElement(expr.accept(this));
        }

        return element;
    }

    @Override
    public Element visit(ClassDifference o) {
        ElementGroup element = new ElementGroup();
        List<ClassExpression> operandList = o.getOperandList();

        element.addElement(operandList.get(0).accept(this));

        for (ClassExpression expr : operandList.subList(1, operandList.size())) {
            element.addElement(new ElementMinus(expr.accept(this)));
        }

        return element;
    }

    @Override
    public Element visit(ClassEnumeration o) {
        ElementData element = new ElementData();

        if (subject.isVariable()) {
            //add variable
            element.add(Var.alloc(subject));

            for (InstanceUri instance : o.getOperandList()) {
                Binding binding = BindingFactory.binding(Var.alloc(subject), NodeFactory.createURI(instance.getUri()));

                // add binding
                element.add(binding);
            }
        } else {
            return defaultValue;
        }

        return element;
    }

    @Override
    public Element visit(ClassCardinalityRestriction o) {
        PropertyExpression property = o.getProperty();

        if (!subject.isVariable()) {
            return defaultValue;
        }

        // create a new expression for the input expression's (un)qualified property
        if (o.getQuantifier() != null) {
            if (property instanceof OPropertyExpression) {
                property = new OPropertyRestriction((OPropertyExpression) property, null, (ClassExpression) o.getQuantifier());
            } else {
                property = new DPropertyRestriction((DPropertyExpression) property, null, (Datatype) o.getQuantifier());
            }
        }

        // rewrite the (sub, R/U, ?var) triple by predicate based on the
        // previously created property expression
        Var variable = VarGenerator.generate();
        Triple triple = Triple.create(subject, Node.ANY, variable);
        PredicateBasedRW predicateRW = new PredicateBasedRW();
        Element e = predicateRW.rewrite(triple, property);

        // create the resulted graph pattern (subquery)
        Query query = QueryFactory.create();
        query.setQuerySelectType();
        query.setQueryPattern(e);
        query.addResultVar(subject);
        query.addGroupBy(subject);

        // create the count aggregate on variable ?var
        AggCountVarDistinct count = new AggCountVarDistinct(new ExprVar(variable));
        Expr countExpr = query.allocAggregate(count);

        // create the having expression using the input expression's unary
        // predicate and the count aggregate
        List<Expr> exprList = new ArrayList<Expr>();
        exprList.add(countExpr);
        Expr havingExpr = o.getCardinality().accept(this, exprList);

        // add the having condition to the query
        query.addHavingCondition(havingExpr);

        return new ElementSubQuery(query);
    }

    @Override
    public Element visit(ClassExistPredRestriction o) {
        ElementGroup element = new ElementGroup();
        List<Expr> exprList = new ArrayList<Expr>();

        for (PropertyExpression expr : o.getOperandList()) {
            // generate a new variable and keep it in the list
            Var variable = VarGenerator.generate();
            exprList.add(new ExprVar(variable));

            // rewrite the (sub, R/U, ?var) triple by predicate based on the
            // current property expression in the operand list
            Triple triple = Triple.create(subject, Node.ANY, variable);
            PredicateBasedRW predicateRW = new PredicateBasedRW();
            Element e = predicateRW.rewrite(triple, expr);

            // add the resulted graph pattern to the group graph pattern
            element.addElement(e);
        }

        // create a filter specifying the predicate condition between the
        // variables in the variable list and add it to the group graph pattern
        ElementFilter filter = new ElementFilter(o.getPredicate().accept(this, exprList));
        element.addElement(filter);

        return element;
    }

    @Override
    public Element visit(ClassExistQuantification o) {
        PropertyExpression property = o.getProperty();

        // create a new expression for the input expression's (un)qualified property
        if (o.getQuantifier() != null) {
            if (property instanceof OPropertyExpression) {
                property = new OPropertyRestriction((OPropertyExpression) property, null, (ClassExpression) o.getQuantifier());
            } else {
                property = new DPropertyRestriction((DPropertyExpression) property, null, (Datatype) o.getQuantifier());
            }
        }

        // rewrite the (sub, R/U, ?var) triple by predicate based on the
        // previously created property expression
        Var variable = VarGenerator.generate();
        Triple triple = Triple.create(subject, Node.ANY, variable);
        PredicateBasedRW predicateRW = new PredicateBasedRW();

        return predicateRW.rewrite(triple, property);
    }

    @Override
    public Element visit(InstanceUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, predicate, NodeFactory.createURI(o.getUri()));
        element.addTriple(triple);
        return element;
    }

    public Expr visit(BinaryPredicate p, List<Expr> o) {
        Expr expr;
        Expr expr1 = o.get(0);
        Expr expr2 = o.get(1);

        if (p.getOperator() == Operator.EQUAL) {
            expr = new E_Equals(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER) {
            expr = new E_GreaterThan(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER_EQUAL) {
            expr = new E_GreaterThanOrEqual(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS) {
            expr = new E_LessThan(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS_EQUAL) {
            expr = new E_LessThanOrEqual(expr1, expr2);
        } else {
            expr = new E_NotEquals(expr1, expr2);
        }

        return expr;
    }

    public Expr visit(UnaryPredicate p, List<Expr> o) {
        Expr expr;
        Expr expr1 = o.get(0);
        Expr expr2 = NodeValue.parse(p.getValue().toString());

        if (p.getOperator() == Operator.EQUAL) {
            expr = new E_Equals(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER) {
            expr = new E_GreaterThan(expr1, expr2);
        } else if (p.getOperator() == Operator.GREATER_EQUAL) {
            expr = new E_GreaterThanOrEqual(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS) {
            expr = new E_LessThan(expr1, expr2);
        } else if (p.getOperator() == Operator.LESS_EQUAL) {
            expr = new E_LessThanOrEqual(expr1, expr2);
        } else {
            expr = new E_NotEquals(expr1, expr2);
        }

        return expr;
    }
}
