/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple.schema;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.MappingVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Relation;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassCardinalityRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassEnumeration;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistQuantification;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassMapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceMapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyTransform;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyTransitive;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Simplifies a mapping in order to be used in schema triple pattern rewriting.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class MappingSimplifier implements MappingVisitor<Mapping>, ExpressionVisitor<Object> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MappingSimplifier.class);

    /**
     * Simplifies a mapping in order to be used in schema triple pattern
     * rewriting.
     *
     * @param mapping A mapping to be simplified.
     * @return The simplified mapping.
     */
    public Mapping simplify(Mapping mapping) {
        if (mapping == null) {
            log.warn("Input mapping cannot be null.");
            return null;
        }

        return mapping.accept(this);
    }

    public Mapping visit(ClassMapping o) {
        ClassExpression tempExpr2 = null;

        // simplify the right side expression
        if (o.getExpr2() != null) {
            tempExpr2 = (ClassExpression) o.getExpr2().accept(this);
        }

        // check if the right side expression has been totally erased
        if (tempExpr2 == null) {
            return null;
        }

        if (!tempExpr2.equals(o.getExpr2())) {
            Relation relation = o.getRelation();

            // if the right side expression has been changed the mapping
            // relation needs to be changed respectively

            if (o.getExpr2() instanceof ClassUnion) {
                if (relation == Relation.SUBSUMED) {
                    relation = Relation.UNSPECIFIED;
                } else {
                    relation = Relation.SUBSUMES;
                }
            } else {
                if (relation == Relation.SUBSUMES) {
                    relation = Relation.UNSPECIFIED;
                } else {
                    relation = Relation.SUBSUMED;
                }
            }

            return new ClassMapping(o.getUri(), o.getExpr1(), tempExpr2, relation);
        }

        return o;
    }

    public Mapping visit(OPropertyMapping o) {
        OPropertyExpression tempExpr2 = null;

        // simplify the right side expression
        if (o.getExpr2() != null) {
            tempExpr2 = (OPropertyExpression) o.getExpr2().accept(this);
        }

        // check if the right side expression has been totally erased
        if (tempExpr2 == null) {
            return null;
        }

        if (!tempExpr2.equals(o.getExpr2())) {
            Relation relation = o.getRelation();

            // if the right side expression has been changed the mapping
            // relation needs to be changed respectively

            if (o.getExpr2() instanceof OPropertyUnion) {
                if (relation == Relation.SUBSUMED) {
                    relation = Relation.UNSPECIFIED;
                } else {
                    relation = Relation.SUBSUMES;
                }
            } else {
                if (relation == Relation.SUBSUMES) {
                    relation = Relation.UNSPECIFIED;
                } else {
                    relation = Relation.SUBSUMED;
                }
            }

            return new OPropertyMapping(o.getUri(), o.getExpr1(), tempExpr2, relation);
        }

        return o;
    }

    public Mapping visit(DPropertyMapping o) {
        DPropertyExpression tempExpr2 = null;

        // simplify the right side expression
        if (o.getExpr2() != null) {
            tempExpr2 = (DPropertyExpression) o.getExpr2().accept(this);
        }

        // check if the right side expression has been totally erased
        if (tempExpr2 == null) {
            return null;
        }

        if (!tempExpr2.equals(o.getExpr2())) {
            Relation relation = o.getRelation();

            // if the right side expression has been changed the mapping
            // relation needs to be changed respectively

            if (o.getExpr2() instanceof DPropertyUnion) {
                if (relation == Relation.SUBSUMED) {
                    relation = Relation.UNSPECIFIED;
                } else {
                    relation = Relation.SUBSUMES;
                }
            } else {
                if (relation == Relation.SUBSUMES) {
                    relation = Relation.UNSPECIFIED;
                } else {
                    relation = Relation.SUBSUMED;
                }
            }

            return new DPropertyMapping(o.getUri(), o.getExpr1(), tempExpr2, relation);
        }

        return o;
    }

    public Mapping visit(InstanceMapping o) {
        return o;
    }

    public Object visit(ClassUri o) {
        return o;
    }

    public Object visit(ClassUnion o) {
        List<ClassExpression> tempOperandList = new ArrayList<ClassExpression>();

        for (ClassExpression expr : o.getOperandList()) {
            ClassExpression temp = (ClassExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new ClassUnion(tempOperandList);
        }

        return null;
    }

    public Object visit(ClassIntersection o) {
        List<ClassExpression> tempOperandList = new ArrayList<ClassExpression>();

        for (ClassExpression expr : o.getOperandList()) {
            ClassExpression temp = (ClassExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new ClassIntersection(tempOperandList);
        }

        return null;
    }

    public Object visit(ClassDifference o) {
        ClassExpression tempOperand = null;
        List<ClassExpression> tempOperandList = new ArrayList<ClassExpression>();

        // note that difference is a special operation requiring the first
        // operand to appear in the resulted - simplified expression

        if (o.getOperandList().get(0) != null) {
            tempOperand = (ClassExpression) o.getOperandList().get(0).accept(this);
        }

        if (tempOperand != null) {
            tempOperandList.add(tempOperand);
        } else {
            return null;
        }

        for (ClassExpression expr : o.getOperandList().subList(1, o.getOperandList().size())) {
            ClassExpression temp = (ClassExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new ClassDifference(tempOperandList);
        }

        return null;
    }

    public Object visit(ClassEnumeration o) {
        return null;
    }

    public Object visit(ClassCardinalityRestriction o) {
        return null;
    }

    public Object visit(ClassExistPredRestriction o) {
        return null;
    }

    public Object visit(ClassExistQuantification o) {
        return null;
    }

    public Object visit(OPropertyUri o) {
        return o;
    }

    public Object visit(OPropertyUnion o) {
        List<OPropertyExpression> tempOperandList = new ArrayList<OPropertyExpression>();

        for (OPropertyExpression expr : o.getOperandList()) {
            OPropertyExpression temp = (OPropertyExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new OPropertyUnion(tempOperandList);
        }

        return null;
    }

    public Object visit(OPropertyIntersection o) {
        List<OPropertyExpression> tempOperandList = new ArrayList<OPropertyExpression>();

        for (OPropertyExpression expr : o.getOperandList()) {
            OPropertyExpression temp = (OPropertyExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new OPropertyIntersection(tempOperandList);
        }

        return null;
    }

    public Object visit(OPropertyDifference o) {
        OPropertyExpression tempOperand = null;
        List<OPropertyExpression> tempOperandList = new ArrayList<OPropertyExpression>();

        // note that difference is a special operation requiring the first
        // operand to appear in the resulted - simplified expression

        if (o.getOperandList().get(0) != null) {
            tempOperand = (OPropertyExpression) o.getOperandList().get(0).accept(this);
        }

        if (tempOperand != null) {
            tempOperandList.add(tempOperand);
        } else {
            return null;
        }

        for (OPropertyExpression expr : o.getOperandList().subList(1, o.getOperandList().size())) {
            OPropertyExpression temp = (OPropertyExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add((OPropertyExpression) temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new OPropertyDifference(tempOperandList);
        }

        return null;
    }

    public Object visit(OPropertyComposition o) {
        return null;
    }

    public Object visit(OPropertyInverse o) {
        return null;
    }

    public Object visit(OPropertyTransitive o) {
        return null;
    }

    public Object visit(OPropertyRestriction o) {
        if (o.getProperty() != null) {
            return o.getProperty().accept(this);
        }

        return null;
    }

    public Object visit(OPropertyExistPredRestriction o) {
        return null;
    }

    public Object visit(DPropertyUri o) {
        return o;
    }

    public Object visit(DPropertyUnion o) {
        List<DPropertyExpression> tempOperandList = new ArrayList<DPropertyExpression>();

        for (DPropertyExpression expr : o.getOperandList()) {
            DPropertyExpression temp = (DPropertyExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new DPropertyUnion(tempOperandList);
        }

        return null;
    }

    public Object visit(DPropertyIntersection o) {
        List<DPropertyExpression> tempOperandList = new ArrayList<DPropertyExpression>();

        for (DPropertyExpression expr : o.getOperandList()) {
            DPropertyExpression temp = (DPropertyExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new DPropertyIntersection(tempOperandList);
        }

        return null;
    }

    public Object visit(DPropertyDifference o) {
        DPropertyExpression tempOperand = null;
        List<DPropertyExpression> tempOperandList = new ArrayList<DPropertyExpression>();

        // note that difference is a special operation requiring the first
        // operand to appear in the resulted - simplified expression

        if (o.getOperandList().get(0) != null) {
            tempOperand = (DPropertyExpression) o.getOperandList().get(0).accept(this);
        }

        if (tempOperand != null) {
            tempOperandList.add(tempOperand);
        } else {
            return null;
        }

        for (DPropertyExpression expr : o.getOperandList().subList(1, o.getOperandList().size())) {
            DPropertyExpression temp = (DPropertyExpression) expr.accept(this);

            if (temp != null) {
                tempOperandList.add(temp);
            }
        }

        if (tempOperandList.size() == 1) {
            return tempOperandList.get(0);
        } else if (tempOperandList.size() > 1) {
            return new DPropertyDifference(tempOperandList);
        }

        return null;
    }

    public Object visit(DPropertyComposition o) {
        return null;
    }

    public Object visit(DPropertyTransform o) {
        return null;
    }

    public Object visit(DPropertyRestriction o) {
        if (o.getProperty() != null) {
            return o.getProperty().accept(this);
        }

        return null;
    }

    public Object visit(InstanceUri o) {
        return o;
    }

    public Object visit(Datatype o) {
        return null;
    }
}
