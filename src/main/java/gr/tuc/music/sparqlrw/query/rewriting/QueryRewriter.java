/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryVisitor;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.sparql.core.Prologue;
import com.hp.hpl.jena.vocabulary.DC_11;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.XSD;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import gr.tuc.music.sparqlrw.mapping.model.Ontology;
import gr.tuc.music.sparqlrw.query.rewriting.graph.GraphRewriter;
import java.util.HashMap;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a SPARQL query based on a set of predefined mappings.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class QueryRewriter implements QueryVisitor {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(QueryRewriter.class);

    private Query result;
    private MappingModel model;
    private HashMap<String, List<String>> ontEndpointMap;

    /**
     * Rewrites a SPARQL query based on a set of predefined mappings.
     *
     * @param query A SPARQL query.
     * @param model A mapping model.
     * @return The reformulated query.
     */
    public Query rewrite(Query query, MappingModel model) {
        return rewrite(query, model, new HashMap<String, List<String>>());
    }

    /**
     * Rewrites a SPARQL query based on a set of predefined mappings.
     *
     * @param query          A SPARQL query.
     * @param model          A mapping model.
     * @param ontEndpointMap The endpoints of the integrated data sources. The
     *                       keys are ontology uris and the values SPARQL
     *                       endpoint urls.
     * @return The reformulated query.
     */
    public Query rewrite(Query query, MappingModel model, HashMap<String, List<String>> ontEndpointMap) {

        if ((query == null) || (model == null) || (ontEndpointMap == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        result = null;
        this.model = model;
        this.ontEndpointMap = ontEndpointMap;

        // rewrite input graph pattern
        query.visit(this);

        return result;
    }

    public void startVisit(Query query) {
        result = query.cloneQuery();
    }

    public void visitPrologue(Prologue prologue) {
        // do nothing
    }

    public void visitResultForm(Query query) {
        // do nothing
    }

    public void visitSelectResultForm(Query query) {
        // do nothing
    }

    public void visitConstructResultForm(Query query) {
        // do nothing
    }

    public void visitDescribeResultForm(Query query) {
        // do nothing
    }

    public void visitAskResultForm(Query query) {
        // do nothing
    }

    public void visitDatasetDecl(Query query) {
        // do nothing
    }

    public void visitQueryPattern(Query query) {
        GraphRewriter graphRW = new GraphRewriter();
        result.setQueryPattern(graphRW.rewrite(query.getQueryPattern(), model, ontEndpointMap));
    }

    public void visitGroupBy(Query query) {
        // do nothing
    }

    public void visitHaving(Query query) {
        // do nothing
    }

    public void visitOrderBy(Query query) {
        // do nothing
    }

    public void visitLimit(Query query) {
        // do nothing
    }

    public void visitOffset(Query query) {
        // do nothing
    }

    public void visitValues(Query query) {
        // do nothing
    }

    public void finishVisit(Query query) {
        PrefixMapping prefixMapping = PrefixMapping.Factory.create();
        String queryStr = result.toString();

        if (queryStr.contains(RDFS.getURI())) {
            prefixMapping.setNsPrefix("rdfs", RDFS.getURI());
        }
        if (queryStr.contains(RDF.getURI())) {
            prefixMapping.setNsPrefix("rdf", RDF.getURI());
        }
        if (queryStr.contains(DC_11.getURI())) {
            prefixMapping.setNsPrefix("dc", DC_11.getURI());
        }
        if (queryStr.contains(OWL.getURI())) {
            prefixMapping.setNsPrefix("owl", OWL.getURI());
        }
        if (queryStr.contains(XSD.getURI())) {
            prefixMapping.setNsPrefix("xsd", XSD.getURI());
        }

        int counter = 1;
        for (Ontology ontology : model.getLocalOntologyList()) {
            if (queryStr.contains(ontology.getUri())) {
                prefixMapping.setNsPrefix("ns" + counter, ontology.getUri());
                counter = counter + 1;
            }
        }

        result.setPrefixMapping(prefixMapping);
    }
}
