/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.utils;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.sparql.ARQConstants;

/**
 * A placeholder for some extensively used nodes.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class NodeUtils {

    public static final Node rdfType = NodeFactory.createURI(ARQConstants.rdfPrefix + "type");
    public static final Node rdfsSubClassOf = NodeFactory.createURI(ARQConstants.rdfsPrefix + "subClassOf");
    public static final Node rdfsSubPropertyOf = NodeFactory.createURI(ARQConstants.rdfsPrefix + "subPropertyOf");
    public static final Node owlEquivalentClass = NodeFactory.createURI(ARQConstants.owlPrefix + "equivalentClass");
    public static final Node owlEquivalentProperty = NodeFactory.createURI(ARQConstants.owlPrefix + "equivalentProperty");
    public static final Node owlComplementOf = NodeFactory.createURI(ARQConstants.owlPrefix + "complementOf");
    public static final Node owlDisjointWith = NodeFactory.createURI(ARQConstants.owlPrefix + "disjointWith");
    public static final Node owlSameAs = NodeFactory.createURI(ARQConstants.owlPrefix + "sameAs");
    public static final Node owlDifferentFrom = NodeFactory.createURI(ARQConstants.owlPrefix + "differentFrom");
}
