/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple.schema;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.TriplePath;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import com.hp.hpl.jena.sparql.syntax.ElementVisitorBase;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import gr.tuc.music.sparqlrw.query.rewriting.graph.GraphSimplifier;
import gr.tuc.music.sparqlrw.query.rewriting.triple.TriplePathConverter;
import gr.tuc.music.sparqlrw.query.rewriting.triple.TripleRewriter;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a schema triple pattern based on a set of predefined GAV mappings.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class SchemaTripleRewriter extends ElementVisitorBase implements TripleRewriter {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SchemaTripleRewriter.class);

    private Element result;
    private MappingModel model;

    /**
     * Rewrites a schema triple pattern based on a set of predefined mappings.
     *
     * @param triple A schema triple pattern.
     * @param model  A mapping model.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, MappingModel model) {

        if (triple == null) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        ElementTriplesBlock element = new ElementTriplesBlock();
        element.addTriple(triple);

        return rewrite(element, model);
    }

    /**
     * Rewrites a graph pattern consisted of schema triple patterns based on a
     * set of predefined mappings.
     *
     * @param element A graph pattern.
     * @param model   A mapping model.
     * @return The reformulated graph pattern.
     */
    public Element rewrite(Element element, MappingModel model) {
        GraphSimplifier simplifier = new GraphSimplifier();

        if ((element == null) || (model == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        result = null;
        this.model = model;

        // rewrite input graph pattern
        element.visit(this);

        // simplify the resulted graph pattern by removing any unnecessary brackets.
        return simplifier.simplify(result);
    }

    @Override
    public void visit(ElementTriplesBlock etb) {
        ElementGroup element = new ElementGroup();

        for (Triple triple : etb.getPattern().getList()) {
            // set default value to the rewriting result (no rewriting performed)
            Element e = new ElementTriplesBlock();
            ((ElementTriplesBlock) e).addTriple(triple);

            SchemaTripleRewriter schemaTripleRW = new SchemaTripleRewriter();
            MappingSimplifier simplifier = new MappingSimplifier();
            Node subject = triple.getSubject();
            Node object = triple.getObject();

            if (subject.isURI() && model.hasMapping(subject.getURI())) {
                // simplify the mapping
                Mapping mapping = model.getMapping(subject.getURI());
                mapping = simplifier.simplify(mapping);

                if (mapping != null) {
                    // rewrite the triple based on its subject part
                    SubjectBasedRW subjectRW = new SubjectBasedRW();
                    e = subjectRW.rewrite(triple, mapping);
                }

                // rewrite the resulted graph pattern based on the object part
                e = schemaTripleRW.rewrite(e, model);
            } else if (object.isURI() && model.hasMapping(object.getURI())) {
                // simplify the mapping
                Mapping mapping = model.getMapping(object.getURI());
                mapping = simplifier.simplify(mapping);

                if (mapping != null) {
                    // rewrite the triple based on its object part
                    ObjectBasedRW objectRW = new ObjectBasedRW();
                    e = objectRW.rewrite(triple, mapping);
                }
            }

            element.addElement(e);
        }

        result = element;
    }

    @Override
    public void visit(ElementPathBlock epb) {
        ElementGroup element = new ElementGroup();

        for (TriplePath triple : epb.getPattern().getList()) {
            Element e = null;

            if (triple.getPath() != null) {
                // the triple contains a property path in its predicate position
                TriplePathConverter converter = new TriplePathConverter();

                try {
                    // transform the triple to a graph pattern consisting of
                    // simple triple patterns
                    e = converter.convert(triple);
                } catch (UnsupportedOperationException ex) {
                    log.warn("Skipped triple pattern containing property path: " + triple.toString());
                }
            } else {
                // the triple does not contain a property path in its predicate
                // position but a simple node
                Triple t = Triple.create(triple.getSubject(), triple.getPredicate(), triple.getObject());
                e = new ElementTriplesBlock();
                ((ElementTriplesBlock) e).addTriple(t);
            }

            if (e != null) {
                // rewrite the resulted graph pattern
                e.visit(this);

                // add the result to the group graph pattern
                element.addElement(result);
            }
        }

        result = element;
    }

    @Override
    public void visit(ElementUnion eu) {
        ElementUnion element = new ElementUnion();

        for (Element e : eu.getElements()) {
            e.visit(this);
            element.addElement(result);
        }

        result = element;
    }

    @Override
    public void visit(ElementGroup eg) {
        ElementGroup element = new ElementGroup();

        for (Element e : eg.getElements()) {
            e.visit(this);
            element.addElement(result);
        }

        result = element;
    }
}
