/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple.data;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import gr.tuc.music.sparqlrw.mapping.model.Expression;
import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitorBase;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a data triple pattern based on a mapping of the term appearing in
 * its subject position.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class SubjectBasedRW extends ExpressionVisitorBase<Element> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SubjectBasedRW.class);

    private Node predicate;
    private Node object;

    /**
     * Rewrites a data triple pattern based on a mapping of the term appearing
     * in its subject position.
     *
     * @param triple  A data triple pattern.
     * @param mapping A mapping for the data triple pattern subject part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Mapping mapping) {

        if (mapping == null) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        return rewrite(triple, mapping.getExpr2());
    }

    /**
     * Rewrites a data triple pattern based on a mapping of the term appearing
     * in its subject position.
     *
     * @param triple     A data triple pattern.
     * @param expression A mapping for the data triple pattern subject part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Expression expression) {

        if ((triple == null) || (expression == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        predicate = triple.getPredicate();
        object = triple.getObject();

        // visit the right side expression
        return expression.accept(this);
    }

    @Override
    public Element visit(InstanceUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(NodeFactory.createURI(o.getUri()), predicate, object);
        element.addTriple(triple);
        return element;
    }

}
