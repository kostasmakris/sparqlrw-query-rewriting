/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.graph;

import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.core.TriplePath;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementAssign;
import com.hp.hpl.jena.sparql.syntax.ElementBind;
import com.hp.hpl.jena.sparql.syntax.ElementData;
import com.hp.hpl.jena.sparql.syntax.ElementDataset;
import com.hp.hpl.jena.sparql.syntax.ElementExists;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementMinus;
import com.hp.hpl.jena.sparql.syntax.ElementNamedGraph;
import com.hp.hpl.jena.sparql.syntax.ElementNotExists;
import com.hp.hpl.jena.sparql.syntax.ElementOptional;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;
import com.hp.hpl.jena.sparql.syntax.ElementService;
import com.hp.hpl.jena.sparql.syntax.ElementSubQuery;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import com.hp.hpl.jena.sparql.syntax.ElementVisitor;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Simplifies a graph pattern by removing unnecessary brackets and empty graph
 * patterns, as well as unifying triple blocks.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class GraphSimplifier implements ElementVisitor {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GraphSimplifier.class);

    private Element result;

    /**
     * Simplifies a graph pattern by removing unnecessary brackets and empty
     * graph patterns, as well as unifying triple blocks.
     *
     * @param element A graph pattern.
     * @return The simplified graph pattern.
     */
    public Element simplify(Element element) {

        if (element == null) {
            log.warn("Input parameter cannot be null.");
            return null;
        }

        // initialize class attributes
        result = null;

        // simplify input graph pattern
        element.visit(this);

        return result;
    }

    public void visit(ElementTriplesBlock el) {
        result = el;
    }

    public void visit(ElementPathBlock el) {
        // should never get here
        result = el;
    }

    public void visit(ElementFilter el) {
        result = el;
    }

    public void visit(ElementAssign el) {
        result = el;
    }

    public void visit(ElementBind el) {
        result = el;
    }

    public void visit(ElementData el) {
        result = el;
    }

    public void visit(ElementUnion el) {
        ElementUnion element = new ElementUnion();

        for (Element e : el.getElements()) {
            e.visit(this);
            element.addElement(result);
        }

        result = element;
    }

    public void visit(ElementOptional el) {
        el.getOptionalElement().visit(this);
        result = new ElementOptional(result);
    }

    public void visit(ElementGroup el) {
        if (el.getElements().size() == 1) {
            Element e = el.getElements().get(0);
            e.visit(this);
            return;
        } else if (el.getElements().size() > 1) {
            ElementGroup element = new ElementGroup();

            for (Element e : el.getElements()) {
                e.visit(this);
                element.addElement(result);
            }

            result = unifyBGPs(element);
            return;
        }

        // should never get here
        result = el;
    }

    public void visit(ElementDataset el) {
        el.getPatternElement().visit(this);
        result = new ElementDataset(el.getDataset(), result);
    }

    public void visit(ElementNamedGraph el) {
        el.getElement().visit(this);
        result = new ElementNamedGraph(el.getGraphNameNode(), result);
    }

    public void visit(ElementExists el) {
        el.getElement().visit(this);
        result = new ElementExists(result);
    }

    public void visit(ElementNotExists el) {
        el.getElement().visit(this);
        result = new ElementNotExists(result);
    }

    public void visit(ElementMinus el) {
        el.getMinusElement().visit(this);
        result = new ElementMinus(result);
    }

    public void visit(ElementService el) {
        el.getElement().visit(this);
        result = new ElementService(el.getServiceNode(), result, el.getSilent());
    }

    public void visit(ElementSubQuery el) {
        Query query = el.getQuery();
        query.getQueryPattern().visit(this);
        query.setQueryPattern(result);
        result = new ElementSubQuery(query);
    }

    private ElementGroup unifyBGPs(ElementGroup el) {
        ElementGroup element = new ElementGroup();
        List<ElementTriplesBlock> etbList = new ArrayList<ElementTriplesBlock>();
        List<ElementPathBlock> epbList = new ArrayList<ElementPathBlock>();
        List<Element> otherList = new ArrayList<Element>();

        // categorize the sub-elements of the input group graph pattern
        for (Element temp : el.getElements()) {
            if (temp instanceof ElementTriplesBlock) {
                etbList.add((ElementTriplesBlock) temp);
            } else if (temp instanceof ElementPathBlock) {
                epbList.add((ElementPathBlock) temp);
            } else if (temp instanceof ElementGroup) {
                for (Element e : ((ElementGroup) temp).getElements()) {
                    otherList.add(e);
                }
            } else {
                otherList.add(temp);
            }
        }

        if (!etbList.isEmpty()) {
            ElementTriplesBlock etb = new ElementTriplesBlock();

            for (ElementTriplesBlock temp : etbList) {
                for (Triple triple : temp.getPattern().getList()) {
                    etb.addTriple(triple);
                }
            }

            element.addElement(etb);
        }

        if (!epbList.isEmpty()) {
            // should never get here
            ElementPathBlock epb = new ElementPathBlock();

            for (ElementPathBlock temp : epbList) {
                for (TriplePath triple : temp.getPattern().getList()) {
                    epb.addTriple(triple);
                }
            }

            element.addElement(epb);
        }

        for (Element temp : otherList) {
            element.addElement(temp);
        }

        return element;
    }
}
