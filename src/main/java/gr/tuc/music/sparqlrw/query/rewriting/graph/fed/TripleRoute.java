/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.graph.fed;

import com.hp.hpl.jena.graph.Triple;
import java.util.ArrayList;
import java.util.List;

/**
 * An object used for triple routing in the different integrated data sources.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class TripleRoute {

    private final List<Triple> tripleList;
    private final List<String> endpointList;

    public TripleRoute(Triple triple, List<String> endpointList) {
        tripleList = new ArrayList<Triple>();
        tripleList.add(triple);

        this.endpointList = new ArrayList<String>(endpointList);
    }

    public List<Triple> getTripleList() {
        return tripleList;
    }

    public List<String> getEndpointList() {
        return endpointList;
    }

    /**
     * Given a list of endpoints, it checks whether the object contains exactly
     * the same endpoints or not.
     *
     * @param endpointList A given list of endpoints.
     * @return True in case the object contains exactly the same endpoints,
     *         otherwise false.
     */
    public boolean hasSameEndpoints(List<String> endpointList) {
        return this.endpointList.containsAll(endpointList) && (this.endpointList.size() == endpointList.size());
    }

    /**
     * Adds (if not already present) a given triple to the triple list.
     *
     * @param triple A triple.
     */
    public void addTriple(Triple triple) {
        if (!tripleList.contains(triple)) {
            tripleList.add(triple);
        }
    }

}
