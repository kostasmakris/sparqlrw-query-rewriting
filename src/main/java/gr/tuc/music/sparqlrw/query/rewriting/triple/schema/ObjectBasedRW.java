/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple.schema;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import gr.tuc.music.sparqlrw.mapping.model.Expression;
import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitorBase;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import gr.tuc.music.sparqlrw.query.rewriting.utils.NodeUtils;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a schema triple pattern based on a mapping of the term appearing in
 * its object position.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class ObjectBasedRW extends ExpressionVisitorBase<Element> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ObjectBasedRW.class);

    private ElementTriplesBlock defaultValue;

    private Node subject;
    private Node predicate;

    /**
     * Rewrites a schema triple pattern based on a mapping of the term appearing
     * in its object position.
     *
     * @param triple  A schema triple pattern.
     * @param mapping A mapping for the schema triple pattern object part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Mapping mapping) {

        if (mapping == null) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        return rewrite(triple, mapping.getExpr2());
    }

    /**
     * Rewrites a schema triple pattern based on a mapping of the term appearing
     * in its object position.
     *
     * @param triple     A schema triple pattern.
     * @param expression A mapping for the schema triple pattern object part.
     * @return A graph pattern.
     */
    public Element rewrite(Triple triple, Expression expression) {

        if ((triple == null) || (expression == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        defaultValue = new ElementTriplesBlock();
        defaultValue.addTriple(triple);

        subject = triple.getSubject();
        predicate = triple.getPredicate();

        // visit the right side expression
        return expression.accept(this);
    }

    @Override
    public Element visit(ClassUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, predicate, NodeFactory.createURI(o.getUri()));
        element.addTriple(triple);
        return element;
    }

    @Override
    public Element visit(ClassUnion o) {
        List<Node> predType1 = new ArrayList<Node>();
        predType1.add(NodeUtils.rdfsSubClassOf);

        List<Node> predType2 = new ArrayList<Node>();
        predType2.add(NodeUtils.rdfType);
        predType2.add(NodeUtils.owlEquivalentClass);
        predType2.add(NodeUtils.owlComplementOf);
        predType2.add(NodeUtils.owlDisjointWith);

        if (predType1.contains(predicate)) {
            ElementUnion element = new ElementUnion();
            for (ClassExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else if (predType2.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (ClassExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(ClassIntersection o) {
        List<Node> predType = new ArrayList<Node>();
        predType.add(NodeUtils.rdfType);
        predType.add(NodeUtils.rdfsSubClassOf);
        predType.add(NodeUtils.owlEquivalentClass);
        predType.add(NodeUtils.owlComplementOf);
        predType.add(NodeUtils.owlDisjointWith);

        if (predType.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (ClassExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(ClassDifference o) {
        List<Node> predType1 = new ArrayList<Node>();
        predType1.add(NodeUtils.rdfType);

        List<Node> predType2 = new ArrayList<Node>();
        predType2.add(NodeUtils.owlDisjointWith);

        if (predType1.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (ClassExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else if (predType2.contains(predicate)) {
            return o.getOperandList().get(0).accept(this);
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(OPropertyUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, predicate, NodeFactory.createURI(o.getUri()));
        element.addTriple(triple);
        return element;
    }

    @Override
    public Element visit(OPropertyUnion o) {
        List<Node> predType1 = new ArrayList<Node>();
        predType1.add(NodeUtils.rdfsSubPropertyOf);

        List<Node> predType2 = new ArrayList<Node>();
        predType2.add(NodeUtils.rdfType);
        predType2.add(NodeUtils.owlEquivalentProperty);

        if (predType1.contains(predicate)) {
            ElementUnion element = new ElementUnion();
            for (OPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else if (predType2.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (OPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(OPropertyIntersection o) {
        List<Node> predType = new ArrayList<Node>();
        predType.add(NodeUtils.rdfType);
        predType.add(NodeUtils.rdfsSubPropertyOf);
        predType.add(NodeUtils.owlEquivalentProperty);

        if (predType.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (OPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(OPropertyDifference o) {
        List<Node> predType = new ArrayList<Node>();
        predType.add(NodeUtils.rdfType);

        if (predType.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (OPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(DPropertyUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, predicate, NodeFactory.createURI(o.getUri()));
        element.addTriple(triple);
        return element;
    }

    @Override
    public Element visit(DPropertyUnion o) {
        List<Node> predType1 = new ArrayList<Node>();
        predType1.add(NodeUtils.rdfsSubPropertyOf);

        List<Node> predType2 = new ArrayList<Node>();
        predType2.add(NodeUtils.rdfType);
        predType2.add(NodeUtils.owlEquivalentProperty);

        if (predType1.contains(predicate)) {
            ElementUnion element = new ElementUnion();
            for (DPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else if (predType2.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (DPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(DPropertyIntersection o) {
        List<Node> predType = new ArrayList<Node>();
        predType.add(NodeUtils.rdfType);
        predType.add(NodeUtils.rdfsSubPropertyOf);
        predType.add(NodeUtils.owlEquivalentProperty);

        if (predType.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (DPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(DPropertyDifference o) {
        List<Node> predType = new ArrayList<Node>();
        predType.add(NodeUtils.rdfType);

        if (predType.contains(predicate)) {
            ElementGroup element = new ElementGroup();
            for (DPropertyExpression expr : o.getOperandList()) {
                element.addElement(expr.accept(this));
            }
            return element;
        } else {
            return defaultValue;
        }
    }

    @Override
    public Element visit(InstanceUri o) {
        ElementTriplesBlock element = new ElementTriplesBlock();
        Triple triple = Triple.create(subject, predicate, NodeFactory.createURI(o.getUri()));
        element.addTriple(triple);
        return element;
    }
}
