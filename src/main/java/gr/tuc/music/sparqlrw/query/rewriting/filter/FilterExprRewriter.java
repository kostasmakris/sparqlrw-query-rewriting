/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.filter;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprAggregator;
import com.hp.hpl.jena.sparql.expr.ExprFunction0;
import com.hp.hpl.jena.sparql.expr.ExprFunction1;
import com.hp.hpl.jena.sparql.expr.ExprFunction2;
import com.hp.hpl.jena.sparql.expr.ExprFunction3;
import com.hp.hpl.jena.sparql.expr.ExprFunctionN;
import com.hp.hpl.jena.sparql.expr.ExprFunctionOp;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.expr.ExprVisitorBase;
import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.graph.NodeTransform;
import gr.tuc.music.sparqlrw.mapping.model.Expression;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a filter expression based on a set of predefined mappings.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class FilterExprRewriter extends ExprVisitorBase implements NodeTransform {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(FilterExprRewriter.class);

    private Expr result;
    private MappingModel model;

    /**
     * Rewrites a filter expression based on a set of predefined mappings.
     *
     * @param expr  A filter expression.
     * @param model A mapping model.
     * @return The reformulated filter expression.
     */
    public Expr rewrite(Expr expr, MappingModel model) {

        if ((expr == null) || (model == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        result = null;
        this.model = model;

        // rewrite input expression
        expr.visit(this);

        return result;
    }

    @Override
    public void visit(ExprFunction0 func) {
        result = func.applyNodeTransform(this);
    }

    @Override
    public void visit(ExprFunction1 func) {
        result = func.applyNodeTransform(this);
    }

    @Override
    public void visit(ExprFunction2 func) {
        result = func.applyNodeTransform(this);
    }

    @Override
    public void visit(ExprFunction3 func) {
        result = func.applyNodeTransform(this);
    }

    @Override
    public void visit(ExprFunctionN func) {
        result = func.applyNodeTransform(this);
    }

    @Override
    public void visit(ExprFunctionOp funcOp) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void visit(NodeValue nv) {
        result = nv.applyNodeTransform(this);
    }

    @Override
    public void visit(ExprVar nv) {
        result = nv.applyNodeTransform(this);
    }

    @Override
    public void visit(ExprAggregator eAgg) {
        result = eAgg.applyNodeTransform(this);
    }

    public Node convert(Node node) {
        if (node.isURI()) {
            Mapping mapping = model.getMapping(node.getURI());
            Expression expr2 = mapping.getExpr2();

            if (expr2 instanceof ClassUri) {
                ClassUri expression = (ClassUri) expr2;
                return NodeFactory.createURI(expression.getUri());
            } else if (expr2 instanceof OPropertyUri) {
                OPropertyUri expression = (OPropertyUri) expr2;
                return NodeFactory.createURI(expression.getUri());
            } else if (expr2 instanceof DPropertyUri) {
                DPropertyUri expression = (DPropertyUri) expr2;
                return NodeFactory.createURI(expression.getUri());
            } else if (expr2 instanceof InstanceUri) {
                InstanceUri expression = (InstanceUri) expr2;
                return NodeFactory.createURI(expression.getUri());
            }
        }

        return node;
    }
}
