/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.graph;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.ARQConstants;
import com.hp.hpl.jena.sparql.core.TriplePath;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementAssign;
import com.hp.hpl.jena.sparql.syntax.ElementBind;
import com.hp.hpl.jena.sparql.syntax.ElementData;
import com.hp.hpl.jena.sparql.syntax.ElementDataset;
import com.hp.hpl.jena.sparql.syntax.ElementExists;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementMinus;
import com.hp.hpl.jena.sparql.syntax.ElementNamedGraph;
import com.hp.hpl.jena.sparql.syntax.ElementNotExists;
import com.hp.hpl.jena.sparql.syntax.ElementOptional;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;
import com.hp.hpl.jena.sparql.syntax.ElementService;
import com.hp.hpl.jena.sparql.syntax.ElementSubQuery;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import com.hp.hpl.jena.sparql.syntax.ElementVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassMapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyMapping;
import gr.tuc.music.sparqlrw.query.rewriting.filter.FilterExprRewriter;
import gr.tuc.music.sparqlrw.query.rewriting.graph.fed.GraphFedConverter;
import gr.tuc.music.sparqlrw.query.rewriting.triple.TriplePathConverter;
import gr.tuc.music.sparqlrw.query.rewriting.triple.TripleRewriter;
import gr.tuc.music.sparqlrw.query.rewriting.triple.data.DataTripleRewriter;
import gr.tuc.music.sparqlrw.query.rewriting.triple.schema.SchemaTripleRewriter;
import gr.tuc.music.sparqlrw.query.rewriting.utils.NodeUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Rewrites a graph pattern based on a set of predefined mappings.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class GraphRewriter implements ElementVisitor {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GraphRewriter.class);

    private Element result;
    private MappingModel model;

    /**
     * Rewrites a graph pattern based on a set of predefined mappings.
     *
     * @param element A graph pattern.
     * @param model   A mapping model.
     * @return The reformulated graph pattern.
     */
    public Element rewrite(Element element, MappingModel model) {
        return rewrite(element, model, new HashMap<String, List<String>>());
    }

    /**
     * Rewrites a graph pattern based on a set of predefined mappings.
     *
     * @param element        A graph pattern.
     * @param model          A mapping model.
     * @param ontEndpointMap The endpoints of the integrated data sources. The
     *                       keys are ontology uris and the values SPARQL
     *                       endpoint urls.
     * @return The reformulated graph pattern.
     */
    public Element rewrite(Element element, MappingModel model, HashMap<String, List<String>> ontEndpointMap) {
        GraphSimplifier simplifier = new GraphSimplifier();

        if ((element == null) || (model == null)) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        // initialize class attributes
        result = null;
        this.model = model;

        // rewrite input graph pattern
        element.visit(this);

        // simplify the resulted graph pattern by removing any unnecessary brackets.
        result = simplifier.simplify(result);

        if (!ontEndpointMap.isEmpty()) {
            // transform the reformulated graph pattern to a federated one
            GraphFedConverter converter = new GraphFedConverter();
            result = converter.convert(result, model, ontEndpointMap);
            result = simplifier.simplify(result);
        }

        return result;
    }

    public void visit(ElementTriplesBlock el) {
        ElementGroup element = new ElementGroup();

        for (Triple triple : el.getPattern().getList()) {
            Element e;
            TripleRewriter tripleRW;

            if (isDataTriplePattern(triple)) {
                // rewrite the data triple pattern
                tripleRW = new DataTripleRewriter();
                e = tripleRW.rewrite(triple, model);
            } else if (isSchemaTriplePattern(triple)) {
                // rewrite the schema triple pattern
                tripleRW = new SchemaTripleRewriter();
                e = tripleRW.rewrite(triple, model);
            } else {
                // no rewriting performed
                e = new ElementTriplesBlock();
                ((ElementTriplesBlock) e).addTriple(triple);
            }

            element.addElement(e);
        }

        result = element;
    }

    public void visit(ElementPathBlock el) {
        ElementGroup element = new ElementGroup();

        for (TriplePath triple : el.getPattern().getList()) {
            Element e = null;

            if (triple.getPath() != null) {
                // the triple contains a property path in its predicate position
                TriplePathConverter converter = new TriplePathConverter();

                try {
                    // transform the triple to a graph pattern consisting of
                    // simple triple patterns
                    e = converter.convert(triple);
                } catch (UnsupportedOperationException ex) {
                    log.warn("Skipped triple pattern containing property path: " + triple.toString());
                }
            } else {
                // the triple does not contain a property path in its predicate
                // position but a simple node
                Triple t = Triple.create(triple.getSubject(), triple.getPredicate(), triple.getObject());
                e = new ElementTriplesBlock();
                ((ElementTriplesBlock) e).addTriple(t);
            }

            if (e != null) {
                // rewrite the resulted graph pattern
                e.visit(this);

                // add the result to the group graph pattern
                element.addElement(result);
            }
        }

        result = element;
    }

    public void visit(ElementFilter el) {
        FilterExprRewriter filterRW = new FilterExprRewriter();
        result = new ElementFilter(filterRW.rewrite(el.getExpr(), model));
    }

    public void visit(ElementAssign el) {
        result = el;
    }

    public void visit(ElementBind el) {
        result = el;
    }

    public void visit(ElementData el) {
        result = el;
    }

    public void visit(ElementUnion el) {
        ElementUnion element = new ElementUnion();

        for (Element e : el.getElements()) {
            e.visit(this);
            element.addElement(result);
        }

        result = element;
    }

    public void visit(ElementOptional el) {
        el.getOptionalElement().visit(this);
        result = new ElementOptional(result);
    }

    public void visit(ElementGroup el) {
        ElementGroup element = new ElementGroup();

        for (Element e : el.getElements()) {
            e.visit(this);
            element.addElement(result);
        }

        result = element;
    }

    public void visit(ElementDataset el) {
        el.getPatternElement().visit(this);
        result = new ElementDataset(el.getDataset(), result);
    }

    public void visit(ElementNamedGraph el) {
        el.getElement().visit(this);
        result = new ElementNamedGraph(el.getGraphNameNode(), result);
    }

    public void visit(ElementExists el) {
        el.getElement().visit(this);
        result = new ElementExists(result);
    }

    public void visit(ElementNotExists el) {
        el.getElement().visit(this);
        result = new ElementNotExists(result);
    }

    public void visit(ElementMinus el) {
        el.getMinusElement().visit(this);
        result = new ElementMinus(result);
    }

    public void visit(ElementService el) {
        el.getElement().visit(this);
        result = new ElementService(el.getServiceNode(), result, el.getSilent());
    }

    public void visit(ElementSubQuery el) {
        Query query = el.getQuery().cloneQuery();
        query.getQueryPattern().visit(this);
        query.setQueryPattern(result);
        result = new ElementSubQuery(query);
    }

    /**
     * Checks whether or not a given triple pattern is a data triple pattern.
     *
     * The triple patterns that deal only with data and not schema info are
     * considered to be Data Triple Patterns.
     *
     * @param triple A triple pattern.
     * @return True in case the input triple pattern refers to data information,
     *         otherwise false.
     */
    private boolean isDataTriplePattern(Triple triple) {
        Node predicate = triple.getPredicate();
        Node object = triple.getObject();

        // if the predicate position contains an ontology object/datatype property
        if (predicate.isURI() && !isVocabTerm(predicate)) {
            Mapping mapping = model.getMapping(predicate.getURI());
            if (mapping instanceof PropertyMapping) {
                return true;
            }
        }

        List<Node> predType = new ArrayList<Node>();
        predType.add(NodeUtils.owlSameAs);
        predType.add(NodeUtils.owlDifferentFrom);

        // if the predicate position contains either the owl:sameAs or the
        // owl:differentFrom property
        if (predType.contains(predicate)) {
            return true;
        }

        // if the predicate position contains the rdf:type property
        if (predicate.equals(NodeUtils.rdfType)) {

            // if the object position contains an ontology class
            if (object.isURI()) {
                Mapping mapping = model.getMapping(object.getURI());
                if (mapping instanceof ClassMapping) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks whether or not a given triple pattern is a schema triple pattern.
     *
     * The triple patterns that only deal with schema and not data info are
     * considered to be Schema Triple Patterns.
     *
     * @param triple A triple pattern.
     * @return True in case the input triple pattern refers to schema
     *         information, otherwise false.
     */
    private boolean isSchemaTriplePattern(Triple triple) {
        Node subject = triple.getSubject();
        Node predicate = triple.getPredicate();
        Node object = triple.getObject();

        List<Node> predType = new ArrayList<Node>();
        predType.add(NodeUtils.rdfType);
        predType.add(NodeUtils.owlSameAs);
        predType.add(NodeUtils.owlDifferentFrom);

        // if the predicate position contains a built-in vocabulary term with the
        // exception of rdf:type, owl:sameAs and owl:differentFrom
        if (isVocabTerm(predicate) && !predType.contains(predicate)) {
            return true;
        }

        // if the predicate position contains the rdf:type property and the
        // object part is a built-in vocabulary term
        if (predicate.equals(NodeUtils.rdfType) && (isVocabTerm(object))) {
            return true;
        }

        // if the predicate position contains the rdf:type property
        if (predicate.equals(NodeUtils.rdfType)) {

            // if the subject position contains a class instance
            if (subject.isURI()) {
                Mapping mapping = model.getMapping(subject.getURI());
                if (mapping instanceof InstanceMapping) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks whether or not a given node is a built-in vocabulary term.
     *
     * @param node A node.
     * @return True in case the input node is a built-in vocabulary term,
     *         otherwise false.
     */
    private boolean isVocabTerm(Node node) {
        if (node.isURI()) {
            String uri = node.getURI();
            if (uri.startsWith(ARQConstants.rdfPrefix) || uri.startsWith(ARQConstants.rdfsPrefix) || uri.startsWith(ARQConstants.owlPrefix)) {
                return true;
            }
        }

        return false;
    }
}
