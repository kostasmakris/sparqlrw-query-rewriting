/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.graph.NodeTransformLib;
import gr.tuc.music.sparqlrw.mapping.model.Deserializer;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import gr.tuc.music.sparqlrw.query.rewriting.utils.TestUtils;
import java.io.File;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class QueryRewriterTest extends TestCase {

    public QueryRewriterTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of rewrite method, of class QueryRewriter.
     */
    public void testRewrite() {
        System.out.print("rewrite");

        TestUtils testUtils = new TestUtils();
        Deserializer deserializer = new Deserializer();
        QueryRewriter rewriter = new QueryRewriter();

        File gavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("query.testfiles/gav.xml"));
        File tests = FileUtils.toFile(getClass().getClassLoader().getResource("query.testfiles/query.properties"));

        MappingModel model = deserializer.deserialize(gavMappings);
        loadLocalSchemas(model);

        int counter = 0;
        List<AbstractMap.SimpleEntry<String, String>> entryList = testUtils.readTests(tests);
        HashMap<String, List<String>> ontEndpointMap = testUtils.getOntEndpointMap();
        for (AbstractMap.SimpleEntry<String, String> entry : entryList) {
            Query input = QueryFactory.create(testUtils.applyNsUri(entry.getKey()));
            Query expected = QueryFactory.create(testUtils.applyNsUri(entry.getValue()));
            Query result = rewriter.rewrite(input, model, ontEndpointMap);

            Op expectedOp = NodeTransformLib.transform(testUtils.reset(), Algebra.compile(expected));
            Op resultOp = NodeTransformLib.transform(testUtils.reset(), Algebra.compile(result));

            counter = counter + 1;
            assertEquals(expectedOp, resultOp);
        }

        System.out.println(": " + counter + " cases tested");
    }

    private void loadLocalSchemas(MappingModel model) {
        File source1 = FileUtils.toFile(getClass().getClassLoader().getResource("source1.owl"));
        File source2 = FileUtils.toFile(getClass().getClassLoader().getResource("source2.owl"));

        OWLOntology localOntology1;
        OWLOntology localOntology2;

        try {
            // load local ontology 1
            localOntology1 = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(source1);

            // load local ontology 2
            localOntology2 = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(source2);
        } catch (OWLOntologyCreationException ex) {
            ex.printStackTrace();
            return;
        }

        model.getLocalOntologyList().get(0).setSchema(localOntology1);
        model.getLocalOntologyList().get(1).setSchema(localOntology2);
    }

}
