/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.utils;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.graph.NodeTransform;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class TestUtils implements NodeTransform {

    private int counter = 1;
    private final HashMap<String, Var> varMap = new HashMap<String, Var>();

    private final HashMap<String, String> nsUriMap = new HashMap<String, String>();
    private final HashMap<String, String> uriNsMap = new HashMap<String, String>();

    private final HashMap<String, List<String>> ontEndpointMap = new HashMap<String, List<String>>();

    public HashMap<String, List<String>> getOntEndpointMap() {
        return ontEndpointMap;
    }

    public String applyUriNs(String element) {
        for (Map.Entry<String, String> entry : uriNsMap.entrySet()) {
            element = element.replaceAll(entry.getKey(), entry.getValue() + ":");
        }
        return element;
    }

    public String applyNsUri(String element) {
        for (Map.Entry<String, String> entry : nsUriMap.entrySet()) {
            element = element.replaceAll(entry.getKey() + ":", entry.getValue());
        }
        return element;
    }

    public TestUtils reset() {
        counter = 1;
        varMap.clear();
        return this;
    }

    /**
     * Reads an input properties file containing various data triple pattern
     * rewriting test cases.
     *
     * @param file A properties file containing test cases.
     * @return The test cases in the form of key-value (input - expected result)
     *         pairs.
     */
    public List<AbstractMap.SimpleEntry<String, String>> readTests(File file) {
        List<AbstractMap.SimpleEntry<String, String>> entryList = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
        Properties prop = new Properties();
        InputStream is = null;

        try {
            is = new FileInputStream(file);
            prop.load(is);

            int i = 1;
            for (;;) {
                String ns = prop.getProperty("ns" + i);
                String uri = prop.getProperty("ns" + i + ".uri");

                if ((ns != null) && (uri != null)) {
                    nsUriMap.put(ns, uri);
                    uriNsMap.put(uri, ns);
                } else if ((ns == null) && (uri == null)) {
                    break;
                }

                i = i + 1;
            }

            i = 1;
            for (;;) {
                String endpoint = prop.getProperty("e" + i);
                String ontology = prop.getProperty("e" + i + ".ont");

                if ((endpoint != null) && (ontology != null)) {
                    List<String> endpointList = ontEndpointMap.get(ontology);
                    if (endpointList != null) {
                        endpointList.add(endpoint);
                    } else {
                        endpointList = new ArrayList<String>();
                        endpointList.add(endpoint);
                        ontEndpointMap.put(ontology, endpointList);
                    }
                } else if ((endpoint == null) && (ontology == null)) {
                    break;
                }

                i = i + 1;
            }

            i = 1;
            for (;;) {
                String input = prop.getProperty("case" + i + ".input");
                String output = prop.getProperty("case" + i + ".output");

                if ((input != null) && (output != null)) {
                    entryList.add(new AbstractMap.SimpleEntry(input, output));
                } else if ((input == null) && (output == null)) {
                    break;
                }

                i = i + 1;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return entryList;
    }

    /**
     * Converts variable names into a common format "?v[number]".
     *
     * @param node An element node.
     * @return The converted node.
     */
    public Node convert(Node node) {
        if (node.isVariable()) {
            Var var = varMap.get(node.getName());
            if (var == null) {
                var = Var.alloc("v" + counter++);
                varMap.put(node.getName(), var);
            }

            return var;
        }
        return node;
    }
}
