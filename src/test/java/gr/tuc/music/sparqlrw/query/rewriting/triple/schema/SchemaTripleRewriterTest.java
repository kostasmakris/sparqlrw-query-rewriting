/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.query.rewriting.triple.schema;

import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.syntax.Element;
import gr.tuc.music.sparqlrw.mapping.model.Deserializer;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import gr.tuc.music.sparqlrw.query.rewriting.utils.TestUtils;
import java.io.File;
import java.util.AbstractMap;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class SchemaTripleRewriterTest extends TestCase {

    public SchemaTripleRewriterTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of rewrite method, of class SchemaTripleRewriter.
     */
    public void testRewrite_Element_MappingModel() {
        System.out.print("rewrite");

        TestUtils testUtils = new TestUtils();
        Deserializer deserializer = new Deserializer();
        SchemaTripleRewriter rewriter = new SchemaTripleRewriter();

        File gavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("triple.testfiles/gav.xml"));
        File tests = FileUtils.toFile(getClass().getClassLoader().getResource("triple.testfiles/schema.properties"));
        MappingModel model = deserializer.deserialize(gavMappings);

        int counter = 0;
        List<AbstractMap.SimpleEntry<String, String>> entryList = testUtils.readTests(tests);
        for (AbstractMap.SimpleEntry<String, String> entry : entryList) {
            Element input = QueryFactory.createElement(testUtils.applyNsUri(entry.getKey()));
            Element expected = QueryFactory.createElement(testUtils.applyNsUri(entry.getValue()));
            Element result = rewriter.rewrite(input, model);

            counter = counter + 1;
            assertEquals(Algebra.compile(expected), Algebra.compile(result));
        }

        System.out.println(": " + counter + " cases tested");
    }

}
